@echo off

:: Parameter checking.
if (%1)==() echo. & echo Error: missing Qt version. & goto End
if (%2)==() echo. & echo Error: missing architecture. & goto End

..\qt\configure.bat -prefix C:\SoftwareDevelopment\Qt\%1\%2 -opensource -confirm-license -release -strip -static -static-runtime -nomake examples -nomake tests -nomake tools -skip qt3d -skip qtdeclarative -skip qtdoc -skip qtgamepad -skip qtlocation -skip qtpurchasing -skip qtspeech -skip qttools -skip qtwebengine -skip qtwebview -skip qtwebglplugin -skip qtandroidextras -skip qtcanvas3d -skip qtcharts -skip qtconnectivity -skip qtdatavis3d -skip qtgraphicaleffects -skip qtimageformats -skip qtmacextras -skip qtmultimedia -skip qtnetworkauth -skip qtquickcontrols -skip qtquickcontrols2 -skip qtscript -skip qtscxml -skip qtsensors -skip qtserialbus -skip qtserialport -skip qtsvg -skip qttranslations -skip qtvirtualkeyboard -skip qtwayland -skip qtwebchannel -skip qtwebengine -skip qtwebsockets -skip qtwinextras -skip qtx11extras -skip qtxmlpatterns -skip qtactiveqt -skip qtremoteobjects -no-dbus -no-direct2d -no-opengl -no-angle -no-sql-sqlite -no-sql-odbc -no-directwrite -qt-pcre -qt-libpng -qt-harfbuzz
if %ERRORLEVEL% neq 0 goto End

:End
