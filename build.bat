@echo off

:: Parameter checking.
if "%1"=="" echo. & echo Error: missing Qt version. & exit /b 1
if "%2"=="" echo. & echo Error: missing architecture. & exit /b 1

echo Building qt-%1-%2 library.

echo - removing old build.
rmdir /s /q .build 1>nul 2>&1

echo - extracting source.
mkdir .build
if ERRORLEVEL 0 (
  cd %1
  if ERRORLEVEL 0 (
::    if not exist "qt" (
      call extract.bat
::    )
    cd ..
  )
)
if not ERRORLEVEL 0 (
  echo.
  echo Error: extraction.
  goto End
)

echo - configuring build.
move /y %1\qt .
copy /y %1\configure.bat .build >nul
if ERRORLEVEL 0 (
  cd .build
  if ERRORLEVEL 0 (
    call configure.bat %1 %2
  )
)
if not ERRORLEVEL 0 (
  echo.
  echo Error: configuration.
  goto End
)

echo - building library.
nmake
if not ERRORLEVEL 0 goto End

echo - installing library.
nmake install

:End
cd ..
