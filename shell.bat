@echo off

:: Parameter checking.
if "%1"=="" echo. & echo Error: missing Qt version. & exit /b 1
if "%2"=="" echo. & echo Error: missing architecture. & exit /b 1

:: Set up architecture variable.
set ARCH=
if "%2"=="x86" set ARCH=x86
:: MVSC 2015,2017 if "%2"=="x86_64" set ARCH=amd64
if "%2"=="x86_64" set ARCH=x86_amd64

if "%ARCH%"=="" echo. & echo Error: unsupported architecture. & exit /b 1

:: Spawn a shell and do the build.
echo Initializing build environment.
echo.
%COMSPEC% /c ""%VCVARS_ROOT_DIR%\vcvarsall.bat" %ARCH% && call build.bat %1 %2"
