@echo off

:: Parameter checking.
if "%1"=="" echo. & echo Error: missing Qt version. & exit /b 1

:: Do the build.
call shell.bat %1 x86_64
